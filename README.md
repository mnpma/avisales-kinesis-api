### aviasales_kinesis
Example of integration an external API and Amazon Kinesis Data Streams
### How to get API Token
1. Sign up to https://app.travelpayouts.com/profile/sources?view=active
   
2. Tools -> generate project -> API and copy token

### How to install and run
See https://docs.aiohttp.org/en/stable/index.html#aiohttp-installation

https://docs.aiohttp.org/en/stable/client_quickstart.html
````shell
pip install aiohttp


````